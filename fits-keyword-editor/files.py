import functools
import logging
import os
import uuid
import numpy as np
from collections import deque
from glob import glob
from pathlib import Path
from astropy.io import fits
from PIL import Image, ImageTk


class FitsFile:

    def __init__(self, path):
        self._id = str(uuid.uuid4())
        self._path = path
        self._event_handlers = {'change': []}

    @property
    def id(self):
        return self._id

    @property
    def path(self):
        return self._path

    @property
    def short_name(self):
        return Path(self._path).name

    @functools.cached_property
    def keywords(self):
        keywords = fits.getheader(self._path)
        return keywords

    @functools.cached_property
    def preview(self):
        bpp = self.keywords['BITPIX']
        width = self.keywords['NAXIS1']
        height = self.keywords['NAXIS2']

        data = fits.getdata(self._path)

        ma = data.max()
        mi = data.min()
        image = np.array(data, copy=True)
        image.clip(mi, ma, out=image)
        image -= mi

        if bpp == 8:
            im = (image / ((ma - mi + 1))).astype(np.uint8)
        elif bpp == 16:
            im = (image / ((ma - mi + 1) / 255.)).astype(np.uint16)

        pil_image = Image.fromarray(im).resize(size=(200, 200), resample=Image.NEAREST)
        img = ImageTk.PhotoImage(image=pil_image, width=width, height=height)

        return img

    def __str__(self):
        return self._path

    def clear_cache(self):
        # Clear the cached keywords if they exist
        self.__dict__.pop('keywords', None)

    def set_keywords(self, keyword_values: dict):
        data, header = fits.getdata(self._path, header=True)

        new_path = self._path
        if not new_path.endswith('.mod.fits'):
            new_path = f'{new_path}.mod.fits'

        for keyword, value in keyword_values.items():
            header[keyword] = value

        fits.writeto(new_path, data, header, overwrite=True)

        self._path = new_path
        self.clear_cache()
        self._emit('change')

    def on_change(self, callback: callable):
        self.on('change', callback)

    def on(self, event: str, callback: callable):
        self._event_handlers[event].append(callback)

    def _emit(self, event, *args, **kwargs):
        for handler in self._event_handlers[event]:
            handler(self, *args, **kwargs)


class Files:

    def __init__(self):
        self._files = deque()
        self._event_handlers = {'add': [], 'remove': [], 'change': []}

    def add(self, path: str):
        if os.path.isdir(path):
            self._add_folder(path)
        elif os.path.isfile(path) and path.lower().endswith('.fits'):
            self._add_file(path)

    def contains(self, path: str):
        return path in [f.path for f in self._files]

    def get(self, file_id: str):
        return next(iter([file for file in self._files if file.id == file_id]), None)

    def _add_folder(self, path: str):
        search_path = Path(path) / "**" / "*.fits"
        files = glob(str(search_path), recursive=True)
        for file in files:
            self._add_file(file)

    def _add_file(self, path: str):
        if self.contains(path):
            logging.warning(f"Not adding file {path} because it is already in the list")
            return
        fits_file = FitsFile(path)
        fits_file.on('change', self._on_file_change)
        self._files.append(fits_file)
        position = self._files.index(fits_file)
        self._emit('add', fits_file, position)

    def _on_file_change(self, sender):
        logging.info(f'Files list detected file change from sender {sender}')
        fits_file = sender
        position = self._files.index(fits_file)
        self._emit('change', fits_file, position)

    def remove(self, file_id: str):
        file = self.get(file_id)
        if not file:
            logging.warning(f"Could not remove file with id {file_id} because it could not be found")
            return
        position = self._files.index(file)
        if position >= 0:
            self._files.remove(file)
            self._emit('remove', file, position)

    def remove_at(self, index: int):
        file = self._files.pop(index)
        self._emit('remove', file, position)

    def on_add(self, callback: callable):
        self.on('add', callback)

    def on_remove(self, callback: callable):
        self.on('remove', callback)

    def on_change(self, callback: callable):
        self.on('change', callback)

    def on(self, event: str, callback: callable):
        self._event_handlers[event].append(callback)

    def _emit(self, event, *args, **kwargs):
        for handler in self._event_handlers[event]:
            handler(self, *args, **kwargs)

    def get_ids(self):
        return [file.id for file in self._files]

    def get_paths(self):
        return [file.path for file in self._files]

    def clear_cache(self):
        for file in self._files:
            file.clear_cache()
