import logging
import threading
import tkinter
from tkinter import messagebox
import tkinterdnd2
import uuid
from tkinter import ttk
from files import Files, FitsFile


class KeywordValue:
    def __init__(self, keyword: str, value: str, value_type: str = 'str'):
        self._id = str(uuid.uuid4())
        self._keyword = keyword.upper()
        self._value = value
        self._type = value_type

    @property
    def id(self):
        return self._id

    @property
    def keyword(self):
        return self._keyword

    @property
    def value(self):
        if self._type == "str":
            return self._value
        if self._type == "float":
            return float(self._value)
        if self._type == "int":
            return int(self._value)
        return None


class WorkDialog(tkinterdnd2.Tk):

    def __init__(self, *args, **kwargs):
        super(WorkDialog, self).__init__(*args, **kwargs)

        self.root = ttk.Frame(self)
        self.root.grid(sticky=tkinter.NSEW, padx=8, pady=8)
        self.root.grid_columnconfigure(0, weight=1, minsize=400)
        self.root.grid_rowconfigure(0, weight=0, minsize=50)
        self.root.grid_rowconfigure(1, weight=0, minsize=50)
        self.root.grid_rowconfigure(2, weight=0, minsize=50)

        self.message = ttk.Label(self.root, text="Working...")
        self.message.grid(column=0, row=0)

        self.progress = ttk.Progressbar(self.root, mode='determinate')
        self.progress.grid(column=0, row=1)

        self.cancel_button = ttk.Button(self.root, text="Cancel", command=self.cancel)
        self.cancel_button.grid(column=0, row=2)

        self.event = threading.Event()

    def cancel(self, *args, **kwargs):
        self.event.set()

    def set_progress(self, value):
        self.progress['value'] = value

    def set_message(self, value):
        self.message['text'] = value


class App(tkinterdnd2.Tk):

    def __init__(self, *args, **kwargs):
        super(App, self).__init__(*args, **kwargs)

        self._files = Files()
        self._files.on_add(self._on_file_added)
        self._files.on_remove(self._on_file_removed)
        self._files.on_change(self._on_file_changed)

        # Style
        style = ttk.Style()
        style.configure("Treeview", font=(None, 8))

        # Window Parameters
        self.title("FITS Keyword Editor")
        self.geometry('800x600')

        # Accept file drops across the entire window
        self.drop_target_register(tkinterdnd2.DND_FILES)
        self.dnd_bind('<<Drop>>', self._on_file_dropped)

        # Window Config
        self.grid_columnconfigure(0, weight=1, minsize=200)
        self.grid_rowconfigure(0, weight=1, minsize=200)

        # Main Grid
        self.root = ttk.Frame(self)
        self.root.grid(sticky=tkinter.NSEW, padx=8, pady=8)
        self.root.grid_columnconfigure(0, weight=3, minsize=300)
        self.root.grid_columnconfigure(1, weight=1, minsize=300)
        self.root.grid_rowconfigure(0, weight=0, minsize=48)
        self.root.grid_rowconfigure(1, weight=1, minsize=250)
        self.root.grid_rowconfigure(2, weight=0, minsize=300)

        # App Title
        self.title = ttk.Label(self.root, text="FITS Keyword Editor", font=(None, 18))
        self.title.grid(column=0, row=0, columnspan=2, sticky=tkinter.NW)

        # File / Folder Drop Target
        self.file_frame = ttk.LabelFrame(self.root, text="FITS Files")
        self.file_frame.grid(column=0, row=1, sticky=tkinter.NSEW, padx=8, pady=8)

        # File Listbox
        self.file_listbox = ttk.Treeview(self.file_frame, columns=('path',), show='headings')
        self.file_listbox.column('path', anchor=tkinter.W, stretch=tkinter.NO, width=800)
        self.file_listbox.heading('path', text="Path")
        self.file_listbox.bind('<Delete>', self._on_delete_file)
        self.file_listbox.bind('<Control-a>', lambda e: self.file_listbox.selection_set(self._files.get_ids()))
        self.file_listbox.bind('<<TreeviewSelect>>', self._on_file_selection_changed)

        # File Listbox Horizontal Scroll
        self._file_listbox_hsb = ttk.Scrollbar(
            self.file_frame, orient=tkinter.HORIZONTAL, command=self.file_listbox.xview)

        # File Listbox Horizontal Scroll
        self._file_listbox_vsb = ttk.Scrollbar(
            self.file_frame, orient=tkinter.VERTICAL, command=self.file_listbox.yview)

        # File Listbox Packing and Linking
        self._file_listbox_hsb.pack(side=tkinter.BOTTOM, fill=tkinter.X)
        self._file_listbox_vsb.pack(side=tkinter.RIGHT, fill=tkinter.Y)
        self.file_listbox.pack(fill=tkinter.BOTH, expand=1)
        self.file_listbox.configure(xscrollcommand=self._file_listbox_hsb.set)
        self.file_listbox.configure(yscrollcommand=self._file_listbox_vsb.set)

        # Keywords Container
        self.keyword_frame = ttk.LabelFrame(self.root, text="Keywords")
        self.keyword_frame.grid(column=1, row=1, sticky=tkinter.NSEW, padx=8, pady=8)

        # Keyword Listbox
        self.keyword_listbox = ttk.Treeview(self.keyword_frame, columns=('name', 'value',), show='headings')
        self.keyword_listbox.column('name', stretch=tkinter.NO, width=120)
        self.keyword_listbox.column('value', stretch=tkinter.NO, width=120)
        self.keyword_listbox.heading('name', text="Name")
        self.keyword_listbox.heading('value', text="Value")

        # Keyword Listbox Horizontal Scroll
        self._keyword_listbox_hsb = ttk.Scrollbar(
            self.keyword_frame, orient=tkinter.HORIZONTAL, command=self.keyword_listbox.xview)

        # Keyword Listbox Vertical Scroll
        self._keyword_listbox_vsb = ttk.Scrollbar(
            self.keyword_frame, orient=tkinter.VERTICAL, command=self.keyword_listbox.yview)

        # Keyword Listbox Packing and Linking
        self._keyword_listbox_hsb.pack(side=tkinter.BOTTOM, fill=tkinter.X)
        self._keyword_listbox_vsb.pack(side=tkinter.RIGHT, fill=tkinter.Y)
        self.keyword_listbox.pack(fill=tkinter.BOTH, expand=1)
        self.keyword_listbox.configure(xscrollcommand=self._keyword_listbox_hsb.set)
        self.keyword_listbox.configure(yscrollcommand=self._keyword_listbox_vsb.set)

        # Keyword Progress Indicator
        self._keyword_progress = ttk.Progressbar(self.keyword_frame, mode='determinate')
        self._keyword_progress.pack(side=tkinter.BOTTOM, fill=tkinter.X)

        # Keyword Editor
        self.editor_frame = ttk.LabelFrame(self.root, text="Keyword Editor")
        self.editor_frame.grid(column=0, row=2, sticky=tkinter.NSEW, padx=8, pady=8)

        self.editor_keyword_variable = tkinter.StringVar(value="")
        self._editor_keyword_label = ttk.Label(self.editor_frame, text="Keyword")
        self._editor_keyword_label.grid(column=0, row=0)
        self.editor_keyword = ttk.Entry(self.editor_frame, textvariable=self.editor_keyword_variable)
        self.editor_keyword.bind('<Return>', self._add_keyword)
        self.editor_keyword.grid(column=1, row=0, columnspan=2)

        self.editor_value_variable = tkinter.StringVar(value="")
        self._editor_value_label = ttk.Label(self.editor_frame, text="Value")
        self._editor_value_label.grid(column=0, row=1)
        self.editor_value = ttk.Entry(self.editor_frame, textvariable=self.editor_value_variable)
        self.editor_value.bind('<Return>', self._add_keyword)
        self.editor_value.grid(column=1, row=1, columnspan=2)

        self.editor_type_variable = tkinter.StringVar(value="str")
        self.editor_type_str = ttk.Radiobutton(self.editor_frame, text="String",
                                               value="str", variable=self.editor_type_variable)
        self.editor_type_str.grid(column=0, row=2)
        self.editor_type_int = ttk.Radiobutton(self.editor_frame, text="Integer",
                                               value="int", variable=self.editor_type_variable)
        self.editor_type_int.grid(column=1, row=2)
        self.editor_type_float = ttk.Radiobutton(
            self.editor_frame, text="Float", value="float", variable=self.editor_type_variable)
        self.editor_type_float.grid(column=2, row=2)

        self.editor_add = ttk.Button(self.editor_frame, text="Add", command=self._add_keyword)
        self.editor_add.grid(column=0, row=3)

        self.editor_keywords = ttk.Treeview(self.editor_frame, columns=(
            'keyword', 'value', 'type',), show='headings', height=4)
        self.editor_keywords.column('keyword', stretch=tkinter.NO, width=120)
        self.editor_keywords.column('value', stretch=tkinter.NO, width=120)
        self.editor_keywords.column('type', stretch=tkinter.NO, width=60)
        self.editor_keywords.heading('keyword', text='Keyword')
        self.editor_keywords.heading('value', text='Value')
        self.editor_keywords.heading('type', text='Type')
        self.editor_keywords.bind('<Delete>', self._on_delete_keyword)
        self.editor_keywords.grid(column=0, row=4, columnspan=2, sticky=tkinter.NSEW, padx=8, pady=8)

        self.editor_update_selection = ttk.Button(
            self.editor_frame, text="Update Selected File(s)", command=self._update_keywords_selected_files)
        self.editor_update_selection.grid(column=1, row=5)

        self.editor_update_all = ttk.Button(self.editor_frame, text="Update All File(s)",
                                            command=self._update_keywords_all_files)
        self.editor_update_all.grid(column=2, row=5)

        # Image Preview Frame
        self.image_preview_frame = ttk.LabelFrame(self.root, text="Image Preview")
        self.image_preview_frame.grid(column=1, row=2, sticky=tkinter.NSEW, padx=8, pady=8)

        # Image Preview
        self.image_preview = tkinter.Canvas(self.image_preview_frame, width=200, height=200, bg="purple")
        self.image_preview.pack()

        # Image Preview Progress Indicator
        self._image_preview_progress = ttk.Progressbar(self.image_preview_frame, mode='indeterminate')
        self._image_preview_progress.pack(side=tkinter.BOTTOM, fill=tkinter.X)

        self._update_keywords_thread = None
        self._update_keywords_event = None

        self._get_preview_thread = None
        self._get_preview_event = None

        self.new_keywords = {}

    def _add_keyword(self, *args, **kwargs):
        keyword = self.editor_keyword_variable.get().upper()
        value = self.editor_value_variable.get()
        value_type = self.editor_type_variable.get()

        if keyword is None or keyword.strip() == "":
            return

        try:
            if value_type == "float":
                value = float(value)
            if value_type == "int":
                value = int(value)
        except ValueError as e:
            logging.exception("Could not cast keyword to requested type")
            messagebox.showerror(
                title="Data Type Error",
                message=f"The value '{value}' cannot be converted to type '{value_type}'")
            return

        if keyword in self.new_keywords.keys():
            kw = self.new_keywords[keyword]
            kw._value = value
            kw._type = value_type
            child = self.editor_keywords.index(kw.id)
            self.editor_keywords.set(kw.id, column='keyword', value=kw.keyword)
            self.editor_keywords.set(kw.id, column='value', value=kw._value)
            self.editor_keywords.set(kw.id, column='type', value=kw._type)

        else:
            kw = KeywordValue(keyword, value, value_type)
            self.editor_keywords.insert('', tkinter.END, kw.id, values=(kw.keyword, kw._value, kw._type))
            self.new_keywords[keyword] = kw

        self.editor_keyword_variable.set("")
        self.editor_value_variable.set("")
        self.editor_keyword.focus()

    def _on_delete_keyword(self, *args, **kwargs):
        keyword_ids = self.editor_keywords.selection()
        logging.info(f"Deleting items: {keyword_ids}")
        keywords = list([kw for kw in self.new_keywords.values() if kw.id in keyword_ids])
        for kw in keywords:
            logging.info(f"Deleting keyword {kw.keyword} = {kw.value}")
            del self.new_keywords[kw.keyword]
        self.editor_keywords.delete(*keyword_ids)

    def _update_keywords_selected_files(self):
        file_ids = self.file_listbox.selection()
        self._update_keywords_to_files_by_id(file_ids)

    def _update_keywords_all_files(self):
        file_ids = self._files.get_ids()
        self._update_keywords_to_files_by_id(file_ids)

    def _update_keywords_to_files_by_id(self, file_ids):

        def do_work():
            dialog = WorkDialog()
            dialog.title = "Updating Keywords"
            dialog.grab_set()

            dialog.attributes('-topmost', True)
            dialog.update()

            files = list([self._files.get(file_id) for file_id in file_ids])
            keyword_values = {kw.keyword: kw.value for kw in self.new_keywords.values()}
            logging.info("Updating Keywords to Files")
            logging.info(f"Files are: {files}")
            logging.info(f"Keywords are: {keyword_values}")

            file_count = len(files)
            i = 0
            for file in files:
                if dialog.event.is_set():
                    break

                dialog.set_message(f"Updating {file.short_name} ({i+1} of {file_count})")
                new_progress_value = (float(i) / float(file_count)) * 100
                dialog.set_progress(new_progress_value)
                dialog.update()

                file.set_keywords(keyword_values)
                i += 1

            dialog.grab_release()
            dialog.destroy()

        thread = threading.Thread(target=do_work, daemon=True)
        thread.run()

    def _on_file_dropped(self, event):
        """ When a file or folder is drag-dropped onto the window """
        data = event.data

        if data.startswith('{') and data.endswith('}'):
            paths = data.split('} {')
            paths[0] = paths[0][1:]
            paths[-1] = paths[-1][:-1]
        else:
            paths = [data]

        for path in paths:
            self._files.add(path)

    def _on_delete_file(self, event):
        """ When the Delete key is pressed on the file list box """
        file_ids = self.file_listbox.selection()
        for file_id in file_ids:
            self._files.remove(file_id)

    def _on_file_added(self, sender, fits_file: FitsFile, index: int):
        """ When a file is added to the backing file list """
        def handle_file_added():
            self.file_listbox.insert('', index, fits_file.id, values=(fits_file.path, ))
        self.after_idle(handle_file_added)

    def _on_file_removed(self, sender, fits_file: FitsFile, index: int):
        """ When a file is removed from the backing file list """
        def handle_file_deleted():
            self.file_listbox.delete(fits_file.id)

        self.after_idle(handle_file_deleted)

    def _on_file_changed(self, sender, fits_file: FitsFile, index: int):
        """ When a file in the backing file list changes """

        def handle_file_changed():
            logging.info(f"File changed in the backend: '{fits_file.path}' at index {index}")
            self.file_listbox.set(fits_file.id, column='path', value=fits_file.path)

            if fits_file.id in self.file_listbox.selection():
                self._update_selected_keywords()
                self._update_image_preview()

        self.after_idle(handle_file_changed)

    def _on_file_selection_changed(self, event):
        """ When the selected items in the file list change """
        self._update_selected_keywords()
        self._update_image_preview()

    def set_keyword_progress(self, value: int):
        def do_update():
            self._keyword_progress['value'] = value
        self.after_idle(do_update)

    def _update_selected_keywords(self):

        def do_update_keywords(evt: threading.Event):
            self._keyword_progress['value'] = 0
            # self._keyword_progress['text'] = 'Updating Keywords'

            # 1. Gather all keywords for the selected file(s)
            keyword_values = {}
            file_ids = self.file_listbox.selection()
            file_count = float(len(file_ids))
            i = 0

            for file_id in file_ids:
                if evt.is_set():
                    self.set_keyword_progress(0)
                    # self._keyword_progress['text'] = ''
                    return

                file = self._files.get(file_id)

                self.set_keyword_progress((i / file_count) * 70.0)
                # self._keyword_progress['text'] = f'Updating Keywords: {file.short_name}'
                for name, value in file.keywords.items():
                    if name not in keyword_values:
                        keyword_values[name] = set()
                    keyword_values[name].add(value)
                i += 1

            if evt.is_set():
                self.set_keyword_progress(0)
                # self._keyword_progress['text'] = ''
                return

            # 2. Gather all unique and non-unique keywords
            self.set_keyword_progress(80)
            # self._keyword_progress['text'] = 'Gathering Keyword Values'
            unique_keywords = {}
            non_unique_keywords = {}
            for name, values in keyword_values.items():
                if len(values) == 1:
                    unique_keywords[name] = list(values)[0]
                else:
                    unique_keywords[name] = '<Multiple Values>'
                    non_unique_keywords[name] = list(values)

            # 3. Update the keywords list
            self.set_keyword_progress(90)
            # self._keyword_progress['text'] = 'Populating Keyword List'

            def do_update_items():
                self.keyword_listbox.delete(*self.keyword_listbox.get_children())
                for name, value in unique_keywords.items():
                    self.keyword_listbox.insert('', tkinter.END, values=(name, value,))

                self.set_keyword_progress(100)

            self.after_idle(do_update_items)
            # self._keyword_progress['text'] = 'Keywords Up To Date'

        if self._update_keywords_thread is not None:
            self._update_keywords_event.set()

        self.keyword_listbox.delete(*self.keyword_listbox.get_children())

        self._update_keywords_event = threading.Event()
        self._update_keywords_thread = threading.Thread(
            target=do_update_keywords, daemon=True, args=(self._update_keywords_event,))
        self._update_keywords_thread.start()

    def set_image_preview_progress(self, running: bool = True):
        def do_update():
            if running:
                self._image_preview_progress.start(2)
            else:
                self._image_preview_progress.stop()
        self.after_idle(do_update)

    def _update_image_preview(self):

        def do_get_preview(evt: threading.Event):
            self.set_image_preview_progress(True)

            def do_delete():
                self.image_preview.delete(tkinter.ALL)

            self.after_idle(do_delete)

            file_ids = self.file_listbox.selection()
            if len(file_ids) != 1:
                self.set_image_preview_progress(False)
                return

            file = self._files.get(file_ids[0])
            if not file:
                self.set_image_preview_progress(False)
                return

            if evt.is_set():
                self.set_image_preview_progress(False)
                return

            # Long running bit
            preview = file.preview

            if evt.is_set():
                self.set_image_preview_progress(False)
                return

            def do_set_image(preview):
                self.image_preview.delete("all")
                self.image_preview.create_image(200, 200, image=preview, anchor=tkinter.SE)  # anchor=tkinter.NW,
                self.set_image_preview_progress(False)

            self.after_idle(do_set_image, preview)

        if self._get_preview_thread is not None:
            self._get_preview_event.set()

        self.image_preview.delete("all")
        # self.image_preview.create_oval(45, 95, 55, 105, fill="red")
        # self.image_preview.create_oval(95, 95, 105, 105, fill="green")
        # self.image_preview.create_oval(145, 95, 155, 105, fill="blue")

        self._get_preview_event = threading.Event()
        self._get_preview_thread = threading.Thread(target=do_get_preview, daemon=True, args=(self._get_preview_event,))
        self._get_preview_thread.start()
