build:
	.venv/bin/pyinstaller --onefile --additional-hooks-dir=pyinstaller-hooks --collect-all tkinterdnd2 fits-keyword-editor/main.py

clean:
	rm -rf ./build ./dist ./fits_keyword_editor.egg-info ./main.spec
