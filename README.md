# FITS Keyword Editor

A simple user interface to view and edit keywords for FITS files.

This project is currently in development. Certain features may not yet exist
(for example, the editing keywords part).

Use of this software is at your own risk. If you use it to edit FITS files,
it might break your files, so make sure you have backups. You assume all
responsibility for any consequences arising from the use of this software.


# How To Use

## Load Files

Drag-and-drop a folder containing FITS files, or FITS files directly, onto
the main application window.

Remove unwanted files from the FITS file list by selecting the file(s) and then
clicking the `Delete` key on your keyboard.


## View FITS Keywords

Select the file(s) that you want to interrogate (you can use standard selection
modifiers like ctrl and shift to select specific files from the list).

A conglomeration of FITS keywords for the selected files are shown. Any keyword
which appears with one value in one or more files is shown with that value.
Any keyword which has multiple values in one or more files is shown with the
special value `<Multiple Values>`.




# Author

Daniel 'Vector' Kerr <vector@vector.id.au>


# Run

1. Initialize a Python Virtual Environment (`./init-venv.sh`)
2. Activate the Virtual Environment (`source .venv/bin/activate`)
3. Run the program (`python3 fits-keyword-editor/main.py`)
4. Optionally deactivate the Virtual Environment when done (`deactivate`)


# Build

1. Run `make` to perform a `pyinstaller` build with the
   appropriate flags
2. Run `make clean` to clean up build and dist files

